# Creative Development

> 1. Formulation of the problem 
>> identify what is wrong
>
> 2. Analysis of the problem 
>> why is it wrong plus all of the details of the issues
>
> 3.Search and create solutions 
>> what are possible solutions plus there are no bad ideas in a collaborative setting
>
> 4.Decision
>> choose the best solution that logically makes the most sense 
> 4a. Solution specification
>> Iron out any issues with the solution and finalize all in-depth details
>
> 5.Prototype
>> create and test using the outline created in step 4a
>
> 6.Feedback
>> get feedback and make comments 
>
> 7.Final Product
>> create final product with feedback implemented
>
> 8. Reflection
>> reflect on what could be improved and what could be altered
>
> 9. Do it again
