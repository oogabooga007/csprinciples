# definitions

**codesegment**

a part of a file that contains executable code

---

**collaboration**

working with somebody else and communicating

---

**comments**

sections of code that are not run by the compiler

---

**debugging**

going through some code that doesnt work and finding out why and fixing it

---

**event triggerd programming**

programs that are triggerd by events like user input

---

**programin in small incremental protions that makes it easier

---

**iterative development process**

breaking software developmentin to smaller chunks

---

**logic error**

when a section of code returns a value that uas unintended 

---

**overflow error**

when a value is assingned to a list index that doesnt exist


---

**program**

a file that does stuff on youre computer through a complier

---

**program behavior**
an approach and technique for software development, which enables incremental development in a natural way

---

**program input**

arguments that can be passed to a program to change the outcome

---

**program output**

the values that the program passes out

---

**prototype**

a basic first genaration of something that is barebones 

---

**requirements**

values needed for a program to run

---

**runtime error** 

when a program runs forever

---

**syntax error** 

when somthing is spelled wrong

---

**testing**

running a program too see if there is any bugs

---

**user interface**

what the user sees in a program




