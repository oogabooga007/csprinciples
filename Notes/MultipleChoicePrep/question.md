## Practise Problems

[Question Locations](http://apcspths.pbworks.com/w/file/fetch/121964706/Sample%2520Exam%2520Questions.pdf)

(1) A video-streaming Web site uses 32-bit integers to count the number of times each video has
been played. In anticipation of some videos being played more times than can be represented
with 32 bits, the Web site is planning to change to 64-bit integers for the counter. Which of the
following best describes the result of using 64-bit integers instead of 32-bit integers?

- (A)  2 times as many values can be represented.
- (B) 32 times as many values can be represented.
- **(C) 2^32 times as many values can be represented.**
- (D) 32^2 times as many values can be represented.

---

(3)ASCII characters can also be represented by hexadecimal numbers. According to ASCII
character encoding, which of the following letters is represented by the hexadecimal (base 16)
number 56?

- (A) A
- (B) L
- **(C) V**
- (D) Y

---

(6)Which of the following statements describes a limitation of using a computer simulation to
model a real-world object or system?

- (A)Computer simulations can only be built afer the real-world object or system has been
created.
- (B) Computer simulations only run on very powerful computers that are not available to the
general public.
- **(C)  Computer simulations usually make some simplifying assumptions about the real-world
object or system being modeled.**
- (D)  It is difcult to change input parameters or conditions when using computer simulations.
