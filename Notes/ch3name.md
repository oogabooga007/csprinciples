 Chapter 3: notes for numbers

## Assingning a name 


> **Variables**

- a **variaible** is a space in computer memory that can hold a value
- all**variables** have names
- setting a **variables** name is called **assignment** 
- for example in the line `a = 4`, `a` is the **assigned** name for the place in memory and `4`  is what is being stored in it

> **Legal Variable Names**

- There are restrictions on what you can use as a variable name

1. it must start with either an upppercase or lowercase letter or an underscore
2. it can also contaiain numbers **as long as they are not first**
3. they cant be python key words
4. no spaces

-  variables are case sensitive
 
---

## Expressions

- the right hand side of the assignment statement doesnt have to be a value. 
- it can be an arithmetic expression like `2 * 2` this line will be evaluated and the result from that will be assigned to the variable
- you can use all standard mathimatical symbols can be used
- you can also use the percentage symbol as the modulo operator which gives you the remander of a divition operation
- the order that expressions are executed in the same why math expretions are evaluated 
