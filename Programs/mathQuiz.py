from random import randint

difficulty = input ("how difficult do you want the quiz to be (Easy,Normal,Hard)\n")
length = int(input("how long do you want the quiz to be\n"))

answersCorrect = 0

for i in range(length):

    num1 = randint(1, 10)
    num2 = randint(1, 10)

    if (difficulty == "Normal"):
        num1 *= randint(1, 10)
        num2 *= randint(1, 10)
    elif (difficulty == "Hard"):
        num1 *= randint(5, 15)
        num2 *= randint(5, 15)

    operatorNum = randint(1, 3)

    operator = ""

    if (operatorNum == 1):
        operator = "+"
    elif (operatorNum == 2):
        operator = "-"
    elif (operatorNum == 3):
        operator = "*"

    question = f"what is {num1} {operator} {num2}\n"

    response = int(input(question))

    answer = 0

    if (operatorNum == 1):
        answer = num1+num2
    elif (operatorNum == 2):
        answer = num1-num2
    elif (operatorNum == 3):
        answer = num1*num2
    
    if(response == answer):
        print("correct")
        answersCorrect += 1
    else:
        print(f"incorect the answer is {answer}")
    i += 1

if (answersCorrect <= length/2):
    print(f"you got {answersCorrect} answers correct dumbass")
elif(answersCorrect >= 3 & answersCorrect <= 7):
    print(f"you got {answersCorrect} answers correct")
elif(answersCorrect > 7):
    print(f"you got {answersCorrect} answers correct smartass")
