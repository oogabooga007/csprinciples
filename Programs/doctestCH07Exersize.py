def getSumOfList(Listlen):
    """
    >>> getSumOfList([1,2,3,4,5,6,7,8,9,10])
    45
    """

    sum = 0
    num_list = list(range(1,Listlen))
    for number in num_list:
        sum += number
        print(sum)
    return sum

print(getSumOfList(10))

if __name__ == "__main__":
    import doctest

    doctest.testmod()
