from gasp import *
import random
    
class Player():

    teleport = True

    def setPlayer():
        sx = random.randint(0,63)
        sy = random.randint(0,47)
        return (sx, sy)
 
    xy = setPlayer()
    print(type(xy))
    x = xy[0]
    y = xy[1]

    def movePlayer(self, key):

        if key == 's' and self.teleport:
            self.x = random.randint(0,63)
            self.y = random.randint(0,47)
            self.teleport = False
        if key == 'w':
            self.y += 2
        if key == 'x':
            self.y -= 2
        if key == 'd':
            self.x += 2
        if key == 'a':
            self.x -= 2
        if key == 'e':
            self.y += 1
            self.x += 1
        if key == 'q':
            self.x -= 1
            self.y += 1
        if key == 'z':
            self.x -= 1
            self.y -= 1
        if key == 'c':
            self.x += 1
            self.y -= 1


class RobotList():

    p = Player()

    robots = []
    canMove = []
    
    def __init__(self, robotNum):
        
        for i in range(robotNum):
            
            sx = random.randint(0,63)
            sy = random.randint(0,47)
            
            self.robots.append([sx,sy])

            self.canMove.append(True)


    def checkPositions(self):
        
        # from chatGPT
        
        for i, num in enumerate(self.robots):

            if self.robots.count(num) > 1:
                self.canMove[i] = "False"
        
    def moveRobot(self):

        for i in range(len(self.robots)):

            if(self.canMove[i] == True):
                
                if (p.x > self.robots[i][0]):
                    self.robots[i][0] += 1
                elif (p.x < self.robots[i][0]):
                    self.robots[i][0] -= 1
                    
                if (p.y > self.robots[i][1]):
                    self.robots[i][1] += 1
                elif (p.y < self.robots[i][1]):
                    self.robots[i][1] -= 1

            else:
                continue
            
    def playerDead(self):
        """
        """

        for i in range(len(self.robots)):
            if (self.robots[i][0] == p.x and self.robots[i][1] == p.y):
                return True
        return False


def movePlayer():
        
    key = update_when('key_pressed')
    p.movePlayer(key)
        

def Draw():
        
    clear_screen()
        
    Circle((10 * p.x + 5, 10 * p.y + 5), 5, filled=True)

    for i in range(robotNum):
        if r.canMove[i]:
            Box((10*r.robots[i][0], 10*r.robots[i][1]), 10, 10, filled=False)
        else:
            Box((10*r.robots[i][0], 10*r.robots[i][1]), 10, 10, filled=True)


def isOver():

    counter = 0
    
    for i in range(len(r.canMove)):
        if r.canMove[i] == True:
            counter += 1
            break
        if counter == 0:
            print("l")
            return (True, True)
            
    if (r.playerDead()):
        didWin = False
        return (True, False)
    return(False, False)

begin_graphics()

done = False

robotNum = 15

moveNum = 0


if __name__ == '__main__':
    
    p = Player()
    r = RobotList(robotNum)

    Draw()

    while not done:


        movePlayer()
        
        r.moveRobot()
        
        r.checkPositions()

        Draw()

        moveNum += 1
        print(moveNum)

        whenOver = isOver()
        over = whenOver[0]
        didWin = whenOver[1]

        if over:
            done = True

    if didWin:
        print(f"YOU WIN, IT LASTED {moveNum} MOVES")
        
    else:
        print(f"YOU LOSE, IT LASTED {moveNum} MOVES")

        
        
